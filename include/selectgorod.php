<?php
$arGorods["moskva"]='Москва';
$arGorods["ulyanovsk"]='Ульяновск';
$arGorods["podolsk"]='Подольск';
$arGorods["reutov"]='Реутов';
$arGorods["cherdakli"]='Чердаклы';
$arGorods["maina"]='Майна';
$gorodkey = (string)filter_input(INPUT_GET, 'gorod'); 
if(!empty($gorodkey)){
$selectedgorod = $arGorods[$gorodkey];
}else{
$selectedgorod = $arGorods['moskva'];
	 }
?>

<div >
<span id="selgorod" class="btn btn-primary css_popup" >
<?php echo $selectedgorod ?>
</span>
</div>
<div id="gorodModal" style="display:none;">
   <h4 class="modal-title">Выберите ваш город</h4>

<ul class="list-group" >
<li class="list-group-item"><a style="font-weight:bold;" href="?gorod=moskva">Москва</a></li>
<li class="list-group-item"><a href="?gorod=podolsk">Подольск</a></li>
<li class="list-group-item"><a href="?gorod=reutov">Реутов</a></li>
<li class="list-group-item"><a style="font-weight:bold;" href="?gorod=ulyanovsk">Ульяновск</a></li>
<li class="list-group-item"><a href="?gorod=maina">Майна</a></li>
<li class="list-group-item"><a href="?gorod=cherdakli">Чердаклы</a></li>
</ul>
</div>
<script>
   window.BXDEBUG = true;
BX.ready(function(){
   var oPopup = new BX.PopupWindow('call_feedback', window.body, {
      autoHide : true,
      offsetTop : 1,
      offsetLeft : 0,
      lightShadow : true,
      closeIcon : true,
      closeByEsc : true,
      overlay: {
         backgroundColor: 'green', opacity: '80'
      }
   });
   oPopup.setContent(BX('gorodModal'));
   BX.bindDelegate(
      document.body, 'click', {className: 'css_popup' },
         BX.proxy(function(e){
            if(!e)
               e = window.event;
            oPopup.show();
            return BX.PreventDefault(e);
         }, oPopup)
   );
   
   
});
</script>