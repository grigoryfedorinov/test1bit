<?php

define('OPT_GROUP_ID', 9);
define('ROZN_GROUP_ID', 10);

class skladinfo {

    var $gorod;
    var $gorodkey;
    var $usergroups;

    public function getskladi() {
        $arSkladi['moskva'] = 1;
        $arSkladi["ulyanovsk"] = 2;
        $arSkladi["podolsk"] = 3;
        $arSkladi["reutov"] = 4;
        $arSkladi["cherdakli"] = 5;
        $arSkladi["maina"] = 6;
        $arResult = $arSkladi;
        return $arResult;
    }

    public function setgorod() {
        $arGorods["moskva"] = 'Москва';
        $arGorods["ulyanovsk"] = 'Ульяновск';
        $arGorods["podolsk"] = 'Подольск';
        $arGorods["reutov"] = 'Реутов';
        $arGorods["cherdakli"] = 'Чердаклы';
        $arGorods["maina"] = 'Майна';
        $this->gorodkey = (string) filter_input(INPUT_GET, 'gorod');
        if (empty($this->gorodkey)) {
            $this->gorodkey = 'moskva';
        }
        $this->gorod = $arGorods[$this->gorodkey];
    }

    //Массив вида Array(by1=>order1[, by2=>order2 [, ..]]), где by - поле для сортировки, может принимать значения: 
    //CATALOG_STORE_AMOUNT_<идентификатор_склада> - сортировка по количеству товара на конкретном складе 
    //(доступно с версии 15.5.5 модуля Торговый каталог)
    //desc,nulls - по убыванию с пустыми значениями в конце выборки;    
    public function getsort() {
        //Определим город пользоввтеля
        $this->setgorod();
        $skladi = $this->getskladi();
        $sortkey = 'CATALOG_STORE_AMOUNT_' . $skladi[$this->gorodkey];
        $sortval = 'asc,nulls';
        $arResult[$sortkey] = $sortval;
        return $arResult;
    }

    public function isopt() {
        $ret = false;
        if (!empty($this->usergroups)) {
            if (in_array(OPT_GROUP_ID, $this->usergroups)) {
                $ret = true;
            }
        }
        return $ret;
    }

    public function isrozn() {
        $ret = false;
        if (!empty($this->usergroups)) {
            if (in_array(ROZN_GROUP_ID, $this->usergroups)) {
                $ret = true;
            }
        }
        return $ret;
    }

    public function setusergroups() {
        if ($GLOBALS['USER']->IsAuthorized()) {
            $this->usergroups = $GLOBALS['USER']->GetUserGroupArray();
        }
    }

    // Массив вида array("фильтруемое поле"=>"значения фильтра" [, ...]). "фильтруемое поле" может принимать значения: 
    // CATALOG_STORE_AMOUNT_<идентификатор_склада> - фильтрация по наличию товара на конкретном складе 
    // (доступно с версии 15.0.2 модуля Торговый каталог). 
    // В качестве значения фильтр принимает количество товара на складе либо false.
    public function getroznfilter() {
        $ret = null;
        $this->setusergroups();
        if ($this->isrozn()) {
            $this->setgorod();
            $skladi = $this->getskladi();
            $ret = new stdClass();
            $ret->key = '>CATALOG_STORE_AMOUNT_' . $skladi[$this->gorodkey];
            $ret->value = 0;
        }
        return $ret;
    }

    public function printvnalichii() {
        echo '<a class="btn btn-link" href="javascript:void(0)" rel="nofollow">Товар в наличии</a>';
    }

    public function printnetvnalichii() {
        echo '<a class="btn btn-link" href="javascript:void(0)" rel="nofollow">Нет в наличии</a>';
    }

    public function printvnalichiivoblasti() {
        echo '<a class="btn btn-link" href="javascript:void(0)" rel="nofollow">Доставка в течение 1-3 дней</a>';
    }
    
    public function printvnalichiivstolice() {
        echo '<a class="btn btn-link" href="javascript:void(0)" rel="nofollow">Доставка в течение 3 дней</a>';
    }

    public function isstolica() {
        $ret = false;
        $ret = in_array($this->gorodkey, array('moskva', 'ulyanovsk'));
        return $ret;
    }

    public function isoblast() {
        $ret = false;
        $ret = in_array($this->gorodkey, array('podolsk', 'reutov', 'cherdakli', 'maina'));
        return $ret;
    }

    public function getoblastgorodkeys() {
        $arOblastInfo['moskva'] = array('podolsk', 'reutov');
        $arOblastInfo['ulyanovsk'] = array('cherdakli', 'maina');
        $ret = $arOblastInfo[$this->gorodkey];
        return $ret;
    }

    public function getstolicagorodkey() {
        $arStolicaInfo['podolsk'] = 'moskva';
        $arStolicaInfo['reutov'] = 'moskva';
        $arStolicaInfo['cherdakli'] = 'ulyanovsk';
        $arStolicaInfo['maina'] = 'ulyanovsk';
        $ret = $arStolicaInfo[$this->gorodkey];
        return $ret;
    }

    public function getAmount($product_id, $sklad_id) {
        $ret = 0;
        $rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $product_id, 'STORE_ID' => $sklad_id), false, false, array('AMOUNT'));
        if ($arStore = $rsStore->Fetch()) {
            $ret = $arStore['AMOUNT'];
        }
        return $ret;
    }

    public function optlogic($product_id) {
        //1. Наличие в текущем городе
        $this->setgorod();
        $skladi = $this->getskladi();
        $sklad_id = $skladi[$this->gorodkey];
        $cnt_curgorod = $this->getAmount($product_id, $sklad_id);        
        if ($cnt_curgorod > 0) { //Товар в наличии в текущем городе
            $this->printvnalichii();
        } else {            
            if ($this->isstolica()) { //Текущий город столица 
                $oblastgorodkeys = $this->getoblastgorodkeys();
                $sklad_oblast0_id = $skladi[$oblastgorodkeys[0]];
                $cntOblast0 = $this->getAmount($product_id, $sklad_oblast0_id);
                $sklad_oblast1_id = $skladi[$oblastgorodkeys[1]];
                $cntOblast1 = $this->getAmount($product_id, $sklad_oblast1_id);
                $vsego = $cntOblast0 + $cntOblast1;                
                if ($vsego > 0) { //Если есть в области
                    $this->printvnalichiivoblasti();
                }                
                else { //Если в текущей области нет в наличии
                    $this->printnetvnalichii();
                }
            }            
            elseif ($this->isoblast()) { //Текущий город в области
                $stolicagorodkey = $this->getstolicagorodkey();
                $sklad_stolica_id = $skladi[$stolicagorodkey];
                $cntStolica = $this->getAmount($product_id, $sklad_stolica_id);
                if ($cntStolica > 0) {
                    $this->printvnalichiivstolice();
                }                
                else { //Если в текущей области нет в наличии
                    $this->printnetvnalichii();
                }
            }
        }
    }

    public function printnalichie($product_id) {
        $this->setusergroups();
        //Если розничный покупатель
        if ($this->isrozn()) {
            $this->printvnalichii();
        }
        //Если оптовый покупатель
        elseif ($this->isopt()) {
            $this->optlogic($product_id);
        }
    }

}
